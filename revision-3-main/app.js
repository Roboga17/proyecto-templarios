const express = require('express');
const app = express();
const path = require('path');

app.use(express.static(path.join(__dirname,'public')))
app.use(express.static('./views'))


app.get('*',(request , response  )=>{
    response.send("La pagina que esta solicitando no fue encontrada")
});


app.listen(3000)